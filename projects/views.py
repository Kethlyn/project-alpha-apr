from django.shortcuts import render, redirect
from projects.models import Project
from django.contrib.auth.decorators import login_required
from projects.forms import ProjectForm


# Create your views here.
@login_required
def project_list_view(request):
    project = Project.objects.filter(owner=request.user)
    context = {"Project": project}
    return render(request, "projects/list.html", context)


@login_required
def project_details_view(request, id):
    project = Project.objects.get(id=id)
    context = {"Project": project, "tasks": project.tasks.all()}
    return render(request, "projects/details.html", context)


@login_required
def project_create_view(request):
    if request.method == "POST":
        form = ProjectForm(request.POST)
        if form.is_valid():
            receipt = form.save(commit=False)
            receipt.purchaser = request.user
            form.save()
            return redirect("list_projects")
    else:
        form = ProjectForm()
    context = {
        "form": form,
    }
    return render(request, "projects/create.html", context)
