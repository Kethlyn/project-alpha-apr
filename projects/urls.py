from django.urls import path
from projects.views import (
    project_list_view,
    project_details_view,
    project_create_view,
)

urlpatterns = [
    path("", project_list_view, name="list_projects"),
    path("<int:id>/", project_details_view, name="show_project"),
    path("create/", project_create_view, name="create_project"),
]
